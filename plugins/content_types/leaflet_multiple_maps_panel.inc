<?php

/**
 * @file
 * Holds leaflet multiple maps plugin for CTools.
 */


$plugin = array(
  'title' => 'Leaflet multiple maps',
  'no title override' => TRUE,
  'category' => array(t('Leaflet'), -9),
  'required context' => new ctools_context_required(t('Node'), 'node'),
);

/**
 * Ctools content type render callback.
 *
 * Render Map.
 */
function leaflet_multiple_maps_leaflet_multiple_maps_panel_content_type_render($subtype, $conf, $args, $context) {
  $node = $context->data;

  $views = array_filter($conf['views']);
  $views_layers = array_filter($conf['views_layers']);

  foreach ($views_layers as $key => $views_layer) {
    $views_layers[$key] = 'layer';
  }
  foreach ($views as $key => $view) {
    $views[$key] = 'base';
  }

  $map = leaflet_map_get_info($conf['map']);

  if (isset($conf['zoom']['initialZoom']) && $conf['zoom']['initialZoom'] >= 0) {
    $map['settings']['zoom'] = intval($conf['zoom']['initialZoom']);
  }
  if (isset($conf['zoom']['minZoom']) && $conf['zoom']['minZoom'] >= 0) {
    $map['settings']['minZoom'] = intval($conf['zoom']['minZoom']);
  }
  if (isset($conf['zoom']['maxZoom']) && $conf['zoom']['maxZoom'] >= 0) {
    $map['settings']['maxZoom'] = intval($conf['zoom']['maxZoom']);
  }
  if (isset($conf['zoom']['scrollWheelZoom'])) {
    $map['settings']['scrollWheelZoom'] = $conf['zoom']['scrollWheelZoom'];
  }

  $views = array_merge($views, $views_layers);

  $data = array();
  
  foreach ($views as $view => $type) {
    
    $markers = array();

    $view = explode('-', $view);
    $main_view = $view[0];
    $delta = $view[1];

    $views_data = views_get_view_result($main_view, $delta);
    $view_info = views_get_view($main_view);
    $settings = $view_info->display[$delta]->display_options['style_options'];

    $name_field = empty($settings['name_field']) ? NULL : $settings['name_field'];

    $data_source = 'field_' . $settings['data_source'];

    foreach ($views_data as $key => $result) {

      if ($result) {
        $geofield[0] = $result->{$data_source}[0]['raw'];
        $points = leaflet_process_geofield($geofield);

        foreach ($result->_field_data as $key => $entity) {

          $id = $result->{$key};

          $object = entity_load_single($entity['entity_type'], $id);

          $description = '';
          if ($settings['description_field'] == '#rendered_entity') {
            if ($node->nid != $id) {
              $build = entity_view($entity['entity_type'], array($object), $settings['view_mode']);
              $description = drupal_render($build);
            }
          } 
          elseif (!empty($settings['description_field'])) {
            $description = $entity['entity']->{$settings['description_field']};
          } 
          foreach ($points as &$point) {
            
            if ($name_field) {
              $point['label'] = $entity['entity']->{$name_field};
            }
            if (isset($description)) {
              $point['popup'] = $description;
            }
     
            $point['icon'] = $settings['icon'];

            $point['icon']['iconUrl'] = token_replace($point['icon']['iconUrl'], array($entity['entity_type'] => $object));

            $point['entity'] = $object;
          }
        }
      }

      $markers = array_merge($markers, $points);
        
    }

    if ($markers) {
      if ($type == 'layer') {
        $data[] = array(
          'group' => true,
          'features' => $markers,
          'label' => $view_info->display[$delta]->display_title,
        );
        $map['settings']['layerControl'] = TRUE;
      }
      else {
        $data = array_merge($data, $markers);
      }
    }
  }

  $height = empty($conf['height']) ? '400' : $conf['height'];

  $block = new stdClass;
  $block->content = leaflet_build_map($map, $data, $height . 'px');
  $block->title = '';

  return $block;

}

/**
 * Maps configuration form.
 */
function leaflet_multiple_maps_leaflet_multiple_maps_panel_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];
  $views = views_get_all_views();
  foreach ($views as $key => $view) {
    foreach ($view->display as $delta => $display) {
      if ($display->display_options['style_plugin'] == 'leaflet') {
        $options[$key . '-' . $delta] = $view->human_name . ': ' . $display->display_title;
      }
    }
  }

  if (count($options > 1)) {
    $form['views'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Select leaflet views to show on map'), 
      '#options' => $options,
      '#default_value' => !empty($conf['views']) ? $conf['views'] : '',
    );

    $form['views_layers'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Select leaflet views to be shown on map as layers'), 
      '#options' => $options,
      '#default_value' => !empty($conf['views_layers']) ? $conf['views_layers'] : '',
      '#description' => t('Views selected on both places will be duplicated.'),
    );
  } 
  else {
    $form['error'] = array(
      '#markup' => t('You must first create at least 2 views with the views plugin leaflet.'),
    );
  }

  $map_options = array();
  foreach (leaflet_map_get_info() as $key => $map) {
    $map_options[$key] = t('@label', array('@label' => $map['label']));
  }
  
  $form['override_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Override View settings'),
    '#description' => t('This settings will override map settings in the view. All other settings will be collected from the respective views.'),
  );

  $form['override_settings']['map'] = array(
    '#title' => t('Map'),
    '#type' => 'select',
    '#options' => $map_options,
    '#default_value' => !empty($conf['map']) ? $conf['map'] : '',
    '#required' => TRUE,
  );

  $form['override_settings']['height'] = array(
    '#title' => t('Map height'),
    '#type' => 'textfield',
    '#field_suffix' => t('px'),
    '#size' => 4,
    '#default_value' => !empty($conf['height']) ? $conf['height'] : '',
    '#required' => FALSE,
  );

  $form['override_settings']['zoom'] = leaflet_form_elements('zoom', $conf);

  return $form;
}

/**
 * Save configuration.
 */
function leaflet_multiple_maps_leaflet_multiple_maps_panel_content_type_edit_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $form_state['conf'] = $form_state['values'];
}
