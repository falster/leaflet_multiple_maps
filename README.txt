/**
 * @file
 * README file for Leaflet Multiple Maps.
 */

 Leaflet Multiple Maps
 Merge multiple maps into one

 ====================================================

The module enables a ctools plugin that let you create a new map on an panels page.

1. Enable module
2. Go to your panels page and add an instance of this widget.
3. Configure what views you want to add. You must first have created more then one view with the leflet views module.
4. Enjoy your map.
